package io.flywheel.xnat.nrepl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.nrg.framework.annotations.XnatPlugin;
import clojure.java.api.Clojure;
import clojure.lang.IFn;

@XnatPlugin(value="nReplPlugin", name="nREPL")
@Slf4j
public class NReplPlugin {
    public static final int port = 7888;
    
    public NReplPlugin() {
        final IFn require = Clojure.var("clojure.core", "require");
        require.invoke(Clojure.read("nrepl.server"));
        final IFn start = Clojure.var("nrepl.server", "start-server");
	require.invoke(Clojure.read("cider.nrepl"));
	final IFn ciderNReplHandler = Clojure.var("cider.nrepl", "cider-nrepl-handler");
	start.invoke(Clojure.read(":port"), Clojure.read(Integer.toString(port)),
		     Clojure.read(":handler"), ciderNReplHandler);

	log.info("nREPL server started on port {}", port);
    }

    @Bean
    public String nReplPluginMessage() {
	return "nREPL plugin";
    }
}
