# nREPL plugin for XNAT
This plugin embeds an nREPL server for Clojure into XNAT via port 7888.

## Building

Using Java 1.8: `gradle xnatPluginJar`
